#!/bin/bash

python manage.py collectstatic --noinput
python manage.py makemigrations
python manage.py migrate
gunicorn resume_api.wsgi:application -w 2 -b :9021 --timeout 300
