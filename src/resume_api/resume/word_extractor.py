import re
import pymorphy2


morph = pymorphy2.MorphAnalyzer()

ignored_parts = ['NPRO', 'PREP', 'CONJ', 'PRCL', 'INTJ']


def get_sum(obj):
    result = 0

    for key in obj:
        result += obj[key]
    return result


def catch(message):
    word_res = []
    words = re.findall(r'[^\s]+', message, re.UNICODE)

    for word in words:
        morph_words = morph.parse(word)
        for morph_word in morph_words:
            if morph_word.tag.POS in ignored_parts:
                continue

            if morph_word.normal_form not in word_res:
                word_res.append(morph_word.normal_form)
    return word_res