from django.contrib import admin
from resume.models import Resume, Area

admin.site.register(Resume)
admin.site.register(Area)
