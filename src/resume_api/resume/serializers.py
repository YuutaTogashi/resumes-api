from rest_framework import serializers
from resume.models import Resume, Area
from rest_framework_serializer_extensions.fields import HashIdField



class ResumeSerializer(serializers.ModelSerializer):
    id = HashIdField(model=Resume, required=False)
    class Meta:
        model = Resume
        fields = ('owner',
                  'title',
                  'area',
                  'skills',
                  'description',
                  'id')

class AreaSerilizer(serializers.ModelSerializer):
    class Meta:
        model = Area
        fields = ('area',)