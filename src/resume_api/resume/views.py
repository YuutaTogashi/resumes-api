from datetime import datetime

import os
from resume.models import Resume, Area
from resume.serializers import ResumeSerializer, AreaSerilizer
from resume.permissions import IsAuth, get_owner
from resume import HASH_IDS

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from django.conf import settings
from django.http import Http404

from elasticsearch import Elasticsearch
from elasticsearch import ElasticsearchException
from resume.permissions import CsrfExemptSessionAuthentication, BasicAuthentication
from resume.word_extractor import catch


def hash_decode(hash):
    return HASH_IDS.decode(hash)[1]

def get_es():
    if 'ELASTIC_HOST' in os.environ:
        return Elasticsearch([os.environ['ELASTIC_HOST']])
    else:
        return Elasticsearch()

def save_to_elastic(resume, city):
    skills_info = catch(' '.join(resume.skills).lower())
    info = skills_info + catch(resume.title.lower())
    es = get_es()
    doc = {
        'owner': resume.owner,
        'title': resume.title,
        'area': resume.area,
        'description': resume.description,
        'info': info,
        'skills': resume.skills,
        'skills_info': skills_info,
        'timestamp': datetime.now(),
        'city': city
    }
    es.index(index="resumes", id=resume.id, body=doc, doc_type="resume")

def delete_from_elastic(id):
    es = get_es()
    es.delete(index="resumes", id=id, doc_type="resume")

search_rules = {
    'info': lambda mess: (' '.join(catch(mess)), 'should', 1),
    'city': lambda city: (city, 'must',1),
    'area': lambda area: (area, 'must', 1),
    'skills_info': lambda skills: (' '.join(catch(skills)), 'should', 2)
}


class ResumeList(APIView):
    """
    List all snippets, or create a new snippet.
    """
    permission_classes = (IsAuth,)
    authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)
    def get_objects(self, owner):
        try:
            return Resume.objects.filter(owner=owner)
        except Resume.DoesNotExist:
            raise Http404

    def get_object(self, id):
        try:
            return Resume.objects.get(id=id)
        except Resume.DoesNotExist:
            raise Http404

    def get(self, request, format=None):
        user = get_owner(request)
        if not user:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        username = user[settings.OWNER_FIELD]

        resumes = self.get_objects(username)
        serializer = ResumeSerializer(resumes, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        user = get_owner(request)
        if not user:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

        username = user[settings.OWNER_FIELD]
        city = user['city']

        data = request.data
        data['owner'] = username
        serializer = ResumeSerializer(data=request.data)
        if serializer.is_valid():
            resume = serializer.save()
            save_to_elastic(resume, city)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, format=None):
        if 'id' not in request.data:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        resume_id = hash_decode(request.data['id'])
        resume = self.get_object(resume_id)
        user = get_owner(request)
        if user[settings.OWNER_FIELD] != resume.owner:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

        data = request.data
        data.pop('id', None)

        serializer = ResumeSerializer(resume, data=request.data)
        if serializer.is_valid():
            resume = serializer.save()
            save_to_elastic(resume, user['city'])
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


    def delete(self, request, format=None):
        user = get_owner(request)
        if not user:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

        username = user[settings.OWNER_FIELD]

        if 'id' not in request.data:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        resume_id = hash_decode(request.data['id'])
        resume = self.get_object(resume_id)
        if resume.owner != username:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        resume.delete()

        delete_from_elastic(resume_id)
        return Response(status=status.HTTP_204_NO_CONTENT)


class ResumeDetail(APIView):
    """
    Retrieve, update or delete a resume instance.
    """

    def get_object(self, pk):
        try:
            return Resume.objects.get(pk=pk)
        except Resume.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        resume_id = hash_decode(pk)
        snippet = self.get_object(resume_id)
        serializer = ResumeSerializer(snippet)
        return Response(serializer.data)


class SearchResume(APIView):
    authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)

    def post(self, request, format=None):
        template = {
            'query': {
                'bool':{
                    'must': [{
                        'bool':{
                            'should':[]}
                    }]
                }
            }
        }

        data = {'must':[],
                'should':[]
                }

        if 'from' not in request.data and 'size' not in request.data:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        for rule in search_rules:
            if rule not in request.data:
                continue
            tmp = {'match': {}}
            rules = search_rules[rule](request.data[rule])
            tmp['match'][rule] = {}
            tmp['match'][rule]['query'] = rules[0]
            tmp['match'][rule]['boost'] = rules[2]
            data[rules[1]] .append(tmp)
            # data['query']['bool']['must'].append(tmp)
            # data['query']['bool']['must']['match'][rule] = search_rules[rule](request.data[rule])

        template['query']['bool']['must'][0]['bool']['should'] = data['should']
        template['query']['bool']['must'] += data['must']

        if len(data['should']) == 0:
            template['query'] = {'match_all':{}}

        es = get_es()
        try:
            hits = es.search(index='resumes',
                            doc_type='resume',
                            body=template,
                            from_=request.data['from'],
                            size=request.data['size'])['hits']['hits']
        except ElasticsearchException:
            return Response(status=status.HTTP_404_NOT_FOUND)

        res = []
        for hit in hits:
            tmp = hit['_source']
            tmp['id'] = hit['_id']
            try:
                resume = Resume.objects.get(id=tmp['id'])
                serializer = ResumeSerializer(resume)
                tmp['id'] = serializer.data['id']
            except Resume.DoesNotExist:
                es.delete(index="resumes", id=tmp['id'], doc_type="resume")
                continue
            tmp.pop('info', None)
            tmp.pop('timestamp', None)
            tmp.pop('skills_info', None)
            res.append(tmp)

        return Response(res)


class AreasList(APIView):
    authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)

    def get(self, request, format=None):
        areas = Area.objects.all()
        serializer = AreaSerilizer(areas, many=True)
        return Response(serializer.data)

