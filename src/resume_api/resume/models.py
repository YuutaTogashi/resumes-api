from django.db import models
from django.contrib.postgres.fields import ArrayField

class Resume(models.Model):
    owner = models.CharField(max_length=256)
    title = models.CharField(max_length=256)
    area = models.CharField(max_length=256)
    skills = ArrayField(models.CharField(max_length=128))
    description = models.TextField()


class Area(models.Model):
    area = models.CharField(max_length=256)